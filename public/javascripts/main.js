
jsBehaviors.common = function( context )
{
  context = typeof context != 'undefined' ? context : $(document);

  var formObj = $('#test-form');
  var testForm = new pelican( formObj );

  $('select.chosen:not(.chosenEnabled)', context).addClass('chosenEnabled').each(function(){
    var width = $(this).data('chosen-width');
    $(this).chosen({width: (width?width:'200') + "px"});
  });

  // Scroll to anchor
  $('span.scroll-anchor:not(.scrollEnabled)', context).addClass('scrollEnabled').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
  });

  // enable cluetips
  $('span.launch-cluetip:not(.cluetipEnabled)', context).addClass('cluetipEnabled').each(function(){
    var gravity = $(this).data('cluetip-gravity');
    var topOffset = $(this).data('cluetip-topoffset');
    if(!topOffset)
      var topOffset = gravity == 's' ? 0 : 20;
    var offset = $(this).data('cluetip-offset');
    offset = !offset ? 0 : offset;

    $(this).tipsy({
      gravity: gravity,
      title: 'data-cluetip-content',
      html: true,
      topOffset: topOffset,
      offset: offset
    });
  });

  // enable dropit dropdown menus 
  $('.drop-menu:not(.dropEnabled)', context ).addClass('dropEnabled').each(function(){
    var menuObj = $(this);
    menuObj.dropit({
      beforeHide: function(e){
        var targetObj = $(e.target);
        // there was a click inside the submenu, keep the menu open for certain click targets
        if( targetObj.closest('.dropit-submenu').length )
        {
          // checking an input inside the submenu, leave it open
          if( targetObj.is('input[type=text]') || targetObj.is('input[type=checkbox]') || targetObj.hasClass('checkbox-label'))
            return false;
        }

        return true;
      }
    });
  });

  $(document).unbind('dropit_create').bind('dropit_create',function( event , menu ){
    var menuObj = $(menu);
    $( 'li' , menuObj ).each(function(){
      var href = $(this).data('href');

      if( href )
        $(this).bind('click',function(){
          window.location.href = href;
        });
    });
  });  

  // spans and the like next to checkboxes can be clicked to toggle the checkbox
  $('.checkbox-label:not(.clickEnabled)', context).addClass('clickEnabled').each(function(){
    $(this).bind('click',function(){
      $(this).siblings('input[type=checkbox]').trigger('click');
    });
  });

  // enable pickaday select. note the expected format for the value of the input is YYYY-MM-DD
  $('input[type=text].pickaday:not(.pickadayEnabled)', context).addClass('pickadayEnabled').each(function(){

    var minDate = $(this).data('pickaday-mindate');
    if( minDate )
      minDate = moment(minDate).toDate();

    var dateFormat = $(this).data('pickaday-format');
    if(!dateFormat)
      dateFormat = 'MMM Do';

    var defaultDate = $(this).val();

    // this plugin does weird bad things if you try to initialize it more than once on the same DOM element, so make sure here
    var datePik = $(this).data('pickaday');     
    if( datePik )
      datePik.destroy();

    $(this).data('pickaday', 
      new Pikaday({ 
        field: $(this)[0], 
        yearRange: 120 , 
        format: dateFormat,
        minDate: minDate
      })
    );

    if( defaultDate )
    {
      var dateParts = defaultDate.split('-');
      $(this).data('pickaday').setDate( new Date( Number(dateParts[0]) , Number(dateParts[1])-1 , Number(dateParts[2]) ));
    }
  });

  // enable timepicker inputs
  $('input[type=text].timepicker', context).each(function(){
    $(this).timepicker();
  });

  // enable phone number input masks
  $('input#phone:not(.maskEnabled)', context ).addClass('maskEnabled').each(function(){
    $(this).mask('(000) 000-0000');
  });

  // feedback modal openers
  $('.open-feedback:not(.feedbackEnabled)', context).addClass('feedbackEnabled').each(function(){
    $(this).bind('click',function(){
      var modalObj = $('#feedback-modal');
      modalObj.modal({
        onShow: function(){
          $('#send-feedback',modalObj).bind('click',function(){
            $.ajax({
              type: 'post',
              data: { content: $('textarea',modalObj).val() },
              url: '/feedback',
              success: function( response , status , xhr ) {
                $('.modal-content',modalObj).html('Your message was sent. Thanks for your feedback.');
              }
            });
          });
        }
      });
    });
  });
}

$(function(){

  $(window).bind('resize.respflag',function(){
    jsData.respFlag = $.respFlag();
  }).trigger('resize.respflag');

  // Jquery simple modal defaults
  $.extend($.modal.defaults, {
    opacity: 60,
    overlayClose: true,
    close: true,
  });

  // Jquery ajax defaults
  $.ajaxSetup({
    timeout: 10 * 1000,
    complete: function(){
      $('img.ajax-loader').remove();
    },
    error: function(){
      $('img.ajax-loader').remove();
      alert("Oye :-(\n\nThere was a problem on our side. Please try again in a few minutes.");
    }
  });  

  if( typeof jsData.flashMessage == 'object' && jsData.flashMessage.message )
    modalSiteMessage( jsData.flashMessage.message , jsData.flashMessage.type );

  // make sure the common function is always first
  jsBehaviors.common();

  $.each( jsBehaviors , function( prop , behavior ){
    if ( typeof behavior == 'function' && prop != 'common' )
      behavior( $(document) );
  });
});


