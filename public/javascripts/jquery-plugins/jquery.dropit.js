/*
 * Dropit v1.0
 * http://dev7studios.com/dropit
 *
 * Copyright 2012, Dev7studios
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */

;(function($) {

    $.fn.dropit = function(method) {

        var methods = {

            init : function(options) {
                this.dropit.settings = $.extend({}, this.dropit.defaults, options);
                return this.each(function() {
                    var $el = $(this),
                         el = this,
                         settings = $.fn.dropit.settings;
                    
                    // Hide initial submenus     
                    $el.addClass('dropit')
                    .find('>'+ settings.triggerParentEl +':has('+ settings.submenuEl +')').addClass('dropit-trigger')
                    .find(settings.submenuEl).addClass('dropit-submenu').hide();

                    function _closeDropit(e)
                    {
                      if( settings.beforeHide.call(this,e) )
                      {
                        $('.dropit-open').removeClass('dropit-open').find('.dropit-submenu').hide();
                        settings.afterHide.call(this,e);  
                      }
                    }
                    
                    $( settings.triggerParentEl +':has('+ settings.submenuEl +') > '+ settings.triggerEl , $el).hoverIntent(function(e){

                        var parentObj = $(this).parents(settings.triggerParentEl);

                        if( parentObj.hasClass('dropit-open')) 
                            return false;

                        settings.beforeHide.call(this,e);

                        $('.dropit-open').removeClass('dropit-open').find('.dropit-submenu').hide();
                        
                        settings.afterHide.call(this);
                        settings.beforeShow.call(this);
                        
                        parentObj.addClass('dropit-open').find(settings.submenuEl).show();
                        
                        settings.afterShow.call(this);
                        
                        return false;
                    },function(){
                        $('ul.dropit-submenu').hoverIntent(function(){

                        },function(e){
                          _closeDropit(e);                          
                        });
  
                    });

                    $(document).unbind('click.dropit').bind('click.dropit',function(e){
                      _closeDropit(e);
                    });
                    
                    $(document).trigger('dropit_create' , $el );
                    
                    settings.afterLoad.call(this);
                });
            }
            
        }

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error( 'Method "' +  method + '" does not exist in dropit plugin!');
        }

    }

    $.fn.dropit.defaults = {
        submenuEl: 'ul', // The submenu element
        triggerEl: 'span.nice-button', // The trigger element
        triggerParentEl: '.drop-menu-content', // The trigger parent element
        afterLoad: function(){}, // Triggers when plugin has loaded
        beforeShow: function(){}, // Triggers before submenu is shown
        afterShow: function(){}, // Triggers after submenu is shown
        beforeHide: function(){ return true; }, // Triggers before submenu is hidden
        afterHide: function(){} // Triggers before submenu is hidden
    }

    $.fn.dropit.settings = {}

})(jQuery);