
// http://stackoverflow.com/questions/1199352/smart-way-to-shorten-long-strings-with-javascript
String.prototype.truncate = String.prototype.truncate ||
      function(n){
          return this.length>n ? this.substr(0,n-1)+'&hellip;' : this;
      };


/*
 * Miscellaneous small JQuery plugins, pasted in here instead of 1 per file
 */
(function ( $ ) {

  $.redirectWithPost = function( uri , data )
  {
    var formObj = $('<form method="post" />');
    formObj.attr('action', uri);
    
    var inputObj;
    $.each( data , function( prop , val ){
      inputObj = $('<input type="hidden" />').attr('name',prop).val(val);
      formObj.append( inputObj );
    });

    formObj.appendTo('body').submit();
  }

  /*
   * If you take a look at _responsive.scss you can see the CSS class definition 
   * for resp-flag, and that different margins are applied at different breakpoints. 
   * This method reads the margin value and returns a user-friendly string that 
   * indicates the responsive breakpoint currently in use. Good for executing JS 
   * based on breakpoints.
   */
  $.respFlag = function()
  {
    var respFlag = Number($('.resp-flag').css('margin-top').replace(/[^0-9]+/gi,''));

    switch( respFlag ) 
    {
      // desktop
      case 0:
        return 'big';
      break;

      // ipads and whatnot, and landscape phone
      case 10:
      case 30:
        return 'medium';
      break;

      // smartphones in portrait mode and whatnot
      case 20:
        return 'small';
      break;
    }
  }

  $.fn.inViewport = function( parentObj )
  {
    var parentObj = typeof parentObj != 'undefined' ? parentObj : $(window);

    var docViewTop = parentObj.scrollTop();
    var docViewBottom = docViewTop + parentObj.height();

    var elemTop = $(this).offset().top;
    var elemBottom = elemTop + $(this).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  };

  $.fn.loadIframeUploader = function( opts )
  {
    var t = $(this);

    var iframeObj = $('<iframe></iframe>').attr('frameborder','0');
    iframeObj.attr({
      src: opts.iframe_src,
    });

    iframeObj.load(function(){

        var formObj = $('<form></form>');
        formObj.attr({
          method: 'post',
          action: opts.action,
          enctype: 'multipart/form-data'
        });

        var formContainerObj = iframeObj.contents().find('.form-container');
        formContainerObj.empty().append( formObj );
        formObj.append("<input type='file' name='"+opts.field_name+"' id='"+opts.field_name+"' />");    

        $('input[type=file]#'+opts.field_name, formObj).bind('change',function(){
          var inputObj = $(this);
          if( inputObj.hasClass('disabled') )
            return;
          inputObj.addClass('disabled');
          $('.footer',t).empty().append(jsData.ajaxLoaderImage);
          formObj.submit();
        });

      });

    if( opts.success )
      window.uploadIframeComplete = opts.success;

    t.prepend( iframeObj );

    return t;
  }

  $.fn.heartRate = function ( opts )
  {
    var default_opts = {
      ajax: false,
      target_input: null,
      rate: null,
      read_only: false
    };
    opts = typeof opts == 'object' ? $.extend( {} , default_opts , opts ) : default_opts;

    var t = $(this);
    var iconObjs = $('.heart-icon',t);
    var activeClass = 'checked';

    if( opts.rate && opts.target_input )
      opts.target_input.val( opts.rate );       

    iconObjs.each(function(i){
      var iconObj = $(this);

      if( opts.rate )
      {
        if( i < Number(opts.rate) )
          iconObj.addClass('checked');
        else
          iconObj.removeClass('checked');
      }

      if( opts.read_only )
        return;

      iconObj.addClass('clickable');
      iconObj.bind('click',function(){
        iconObj.addClass( activeClass );
        iconObj.prevAll('.heart-icon').addClass( activeClass );
        iconObj.nextAll('.heart-icon').removeClass( activeClass );

        var rate = iconObjs.filter( '.'+activeClass ).length;
        $('.hearts-container', t ).data( 'rate' , rate );

        if( opts.target_input )
          opts.target_input.val( rate );
      });
    });

    t.addClass('enabled');
 
    return t;
  }

  /*
   * Remove an element from the DOM and return its entire HTML content as 
   * a string. This is handy when, for example, initializing AgilityJS objects
   * that already exist in the DOM
   */
  $.fn.pullFromDom = function ( ) 
  {
    return $(this).remove().wrap('<div />').parent().html();
  }


  /*
   * Scroll the window to the specified element using animate
   */
  $.fn.scrollHere = function( opts ) 
  {
    var default_opts = {
      speed: 500,
      offset: 0
    };
    opts = typeof opts == 'object' ? $.extend( {} , default_opts , opts ) : default_opts;

    $('html, body').animate({
        scrollTop: $(this).offset().top + opts.offset
    }, opts.speed );
  }


  /* jQuery.values: get or set all of the name/value pairs from child input controls   
   * @argument data {array} If included, will populate all child controls.
   * @returns element if data was provided, or array of values if not
   * http://stackoverflow.com/questions/1489486/jquery-plugin-to-serialize-a-form-and-also-restore-populate-the-form
   */
  $.fn.formValues = function(data) 
  {
    var els = this.find(':input').get();

    if(arguments.length === 0) {
      // return all data
      data = {};

      $.each(els, function() {
          if (this.name && !this.disabled && (this.checked
                          || /select|textarea/i.test(this.nodeName)
                          || /text|hidden|password/i.test(this.type))) {
              if(data[this.name] == undefined){
                  data[this.name] = [];
              }
              data[this.name].push($(this).val());
          }
      });

      return data;

    } else {
      $.each(els, function() {
        if (this.name && data[this.name]) 
        {
          var names = data[this.name];
          var $this = $(this);

          if(Object.prototype.toString.call(names) !== '[object Array]')
              names = [names]; //backwards compat to old version of this code
          
          if(this.type == 'checkbox' || this.type == 'radio') 
          { 
              var val = $this.val();
              var found = false;

              for(var i = 0; i < names.length; i++)
              {
                  if(names[i] == val)
                  {
                    found = true;
                    break;
                  }
              }

              $this.attr("checked", found);

          } else {

            $this.val(names[0]);
          }
        }
      });

      return this;
    }
  };  

}( jQuery ));



function inlineSiteMessage( message , fadeOut )
{
  fadeOut = typeof fadeOut == 'boolean' ? fadeOut : false;

  var messageObj = $('#inline-site-message');
  messageObj.html( '<span>' + message + '</span>' );

  if( fadeOut )
    $('span' , messageObj).delay(5000).fadeOut('fast',function(){
      $(this).remove();
    });
}

function modalSiteMessage( message , messageType )
{
  message = typeof message == 'string' ? message : '';
  messageType = typeof messageType == 'string' ? messageType : 'good';

  var modalObj = ciTemplates.getAdd('modalSiteMessage');
  
  modalObj.modal({
    onShow: function(){
      $('.modal-content',modalObj).html('<div class="site-message '+messageType+'">'+message+'</div>' );
    } 
  });
}

function modalConfirmMessage( message , callback , opts )
{
  var modalObj = ciTemplates.getAdd('modalSiteConfirm');
  var default_opts = {
    title: 'Are you sure?',
    confirm_label: 'Yes',
    cancel_label: 'No'
  };

  opts = typeof opts == 'object' ? opts : {};
  opts = $.extend( {} , default_opts , opts );

  $('.modal-header' , modalObj).html( opts.title );
  $('.modal-footer-buttons .confirm',modalObj).html( opts.confirm_label );
  $('.modal-footer-buttons .cancel',modalObj).html( opts.cancel_label );
  
  $.modal.switchModals( modalObj , {
    onShow: function(){
      $('.modal-content .confirm-message',modalObj).html( message );

      $('.modal-footer-buttons .cancel' , modalObj ).bind('click',function(){
        $.modal.close();
      });

      $('.modal-footer-buttons .confirm', modalObj ).bind('click',function(){
        callback( modalObj );
        $.modal.close();
      });
    }
  });
}

/*
 * Simple class for showing HTML messages in a container object, handy for AJAX call responses, etc
 */
var messager = function( obj )
{
  this.obj = obj;
};
messager.prototype.clean = function()
{
  this.obj.html('');
};
messager.prototype.setMessage = function( message , extraClass , fadeOut )
{
  message = typeof message == 'string' ? message : '';
  fadeOut = typeof fadeOut == 'number' ? fadeOut : 5000;
  extraClass = typeof extraClass == 'string' ? extraClass : '';

  this.obj.html('<span class="message '+extraClass+'">' + message + '</span>');

  if( fadeOut > 0 )
    $('.message' , this.obj ).delay(fadeOut).fadeOut();
};
messager.prototype.badMessage = function( message , fadeOut )
{
  this.setMessage( message , 'error' , fadeOut );
};
messager.prototype.goodMessage = function( message , fadeOut )
{
  this.setMessage( message , 'good' , fadeOut );
};

function ajaxLoadModal( opts )
{
  // open the loader modal
  var modalObj = $('#loader-modal');

  modalObj.modal({
    onShow: function( e ){

      var ajaxOpts = {
        url: opts.url,
        type: opts.type ? opts.type : 'GET',
        dataType: 'html',
        beforeSend: function(){
          $('.modal-content',modalObj).html( '<img src="/images/ajax-loader.gif" class="ajax-load" />' );
        },
        success: function( response ){

          var modalHtml = $('<div />').html( response );
          var modalId = $('.modal' , modalHtml ).attr('id');

          // setup the new modal content          
          modalObj.attr('id',modalId);
          $('.modal-inner',modalObj).html( $('.modal-inner',modalHtml).html() );

          // re-center the modal
          $(window).trigger('resize.simplemodal');

          if( opts.onShow && typeof opts.onShow == 'function' )
            opts.onShow( modalObj );
        }
      };

      if( opts.type == 'POST' && opts.data )
        ajaxOpts.data = opts.data;

      $.ajax( ajaxOpts );
    }
  });
}

function parseQuerystring( str )
{
  var vals = {};
  var str_parts = str.replace(/^#/,'').split('&');
  var val_parts,val;

  for( var x = 0; x < str_parts.length; x++ )
  {
    val_parts = str_parts[ x ].split('=');
    val = val_parts.length == 2 ? decodeURIComponent( val_parts[1] ) : '';
    vals[ val_parts[0] ] = val;
  }
  return vals;
}

function userSignedIn()
{
  return typeof ciData.user.id != 'undefined' && ciData.user.id > 0;
}

function memberSignedIn()
{
  return typeof ciData.member.id != 'undefined' && ciData.member.id > 0;
}

// TODO: Can't be sure of a better place to put this, don't want to use the global scope, but don't really have
// time to rethink my whole approach here.
function openSignup( opts )
{
  opts = $.extend( {} , {
    message: 'Please fill out the form below to create your account. <a href="#" class="open-login">Already have an account?</a>'
  }, opts );

  var modalObj = $('#signup-modal');
  $.modal.switchModals( modalObj , {
    onShow: function(){
      var formObj = $('#welcome-signup-form',modalObj);
      var signupFormApi = new formApi( formObj , { on_success:  function( response ){

        // grep js/controllers/welcome.js for a note on what this is
        if( window.pendingAppointRequest )
        {
          ciData.user = response.updatedModel;
          // replace header links
          var headerObj = $('.header-tools');
          headerObj.html('<a href="/home">' + response.updatedModel.first_name + '</a>');

          window.pendingAppointRequest();
          delete window.pendingAppointRequest;
          return;        
        }

        // regular sign-up process, reload to get to user dashbaord
        window.location.reload();
      }});

      $('input#phone' , formObj).mask('(000) 000-0000');
      $('.form-message').html( opts.message );
      ciBehaviors.pageWelcome( modalObj );
    }
  });
}