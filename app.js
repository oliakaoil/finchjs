var config = require('./config');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jade = require('jade');

var app = express();

process.title = 'node-finchjs';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// add core client-side JS to response
app.use(function (req, res, next) {

  res.locals.testPath = 'foo';

  res.locals.jsData = JSON.stringify({ labels: config.labels });
  res.locals.javascripts = config.core_javascript;
  res.locals.javascripts = res.locals.javascripts.concat( config.module_assets );
  res.locals.javascripts.push('main.js');

  next();
});

// HMVC: Workaround to make view files work in subfolders
// http://stackoverflow.com/questions/12132978/use-a-variable-in-a-jade-include
app.use(function( req, res, next ){

  app.layoutRender = function( content_path , view_vars ) {

    content_path += '.jade';

    view_vars = view_vars ? view_vars : {};
    view_vars.templateRender = jade.renderFile;
    view_vars.content_path = content_path;

    res.render('layout', view_vars);
  };

  next();
});


// HMVC: Load routes from app modules
config.app_modules.forEach(function( app_module ){
  var routes = require('./app_modules/' + app_module + '/routes');
  routes( app );
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') 
{
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;