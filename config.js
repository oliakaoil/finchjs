
var config = {};

config.labels = 
{
  "siteName": "News Site",
  "siteUrl": "radio:8081"
};

config.app_modules = 
[
 'front'
];


config.core_javascript = 
[
 'moment.js',
 'jquery-1.10.2.min.js',
 
 'jquery-plugins/jquery.mask.min.js',
 'jquery-plugins/jquery.hoverintent.js',
 'jquery-plugins/jquery.simplemodal.1.4.4.js',
 'jquery-plugins/jquery-tipsy/jquery.tipsy.js',
 'jquery-plugins/jquery.dropit.js',
 'jquery-plugins/jquery-timepicker/jquery.timepicker.min.js',      
 'jquery-plugins/jquery.form.min.js',      

  'pickaday.js',
  'plugins.js'
];

config.module_assets = 
[
'angular/angular.min.js',
'pelican/pelican.js'
];


module.exports = config;