
var forms = require('./forms');
var view_path = __dirname + '/views/';

module.exports = function( app ) {

  app.all('/', function(req, res, next) {

    if( req.body.login )
    {
      res.json(forms.test.get( req.body ));
      return;
    }

    app.layoutRender( view_path + 'index' , { title: 'Expresssss!' , form: forms.test.get() } );
  });


};
