
var Pelican = require('pelican');

var forms = {};


forms.test = new Pelican('test-form');

forms.test.form_build = function( data ) {
  var fields = {};

  fields.username = 
  {
    title: 'Username',
    type: 'text',
    default_value: '',
    placeholder: 'Enter your username',
    required: true
  };

  fields.login = 
  {
    value: 'Sign In',
    type: 'submit'
  };

  return { fields: fields };
}



module.exports = forms;