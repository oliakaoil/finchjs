Overview
==

A customized version of ExpressJS with various additions to create a more "complete" web application framework. 

On the server-side, this includes ExpressJS middleware to make HMVC possible, tight integration with a Drupal-like HTML Form API called Pelican, MySQL support because what's better than NoSQL? SQL, and well-formed model classes with Sequelize. 

On the client-side, some nice core CSS sheets (normalize, skeleton 1200) converted to SASS, a bunch of useful client-side Javascript libraries including JQuery, a well-defined Javascript bootstraping process and namespacing strategy, and AngularJS for the kids.